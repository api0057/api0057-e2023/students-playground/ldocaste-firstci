import pytest
from distances.distance import minimal, hausdorff
import numpy as np

x = np.array([[0, 0], [1, 1]])
y = np.array([[0, 0], [2, 2]])


def test_minimal():
    assert np.isclose(minimal(x, y), 0.0)


def test_hausdorff():
    assert np.isclose(hausdorff(x, y), 1.4142135623730951)
